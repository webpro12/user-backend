import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Products } from './entities/product.entity';

let products: Products[] = [
  { id: 1, name: 'bananabomb', price: 45 },
  { id: 2, name: 'mangobomb', price: 55 },
  { id: 3, name: 'pineapple', price: 30 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Products = {
      id: lastProductId++,
      ...createProductDto, // login , name, password
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    //console.log('user ' + JSON.stringify(users[index]));
    //console.log('update ' + JSON.stringify(updateProductDto));
    const updateProduct: Products = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'bananabomb', price: 45 },
      { id: 2, name: 'mangobomb', price: 55 },
      { id: 3, name: 'pineapple', price: 30 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
